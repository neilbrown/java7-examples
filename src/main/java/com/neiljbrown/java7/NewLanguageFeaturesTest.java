/**
 * https://bitbucket.org/neilbrown/java7-examples
 * 
 * $Id:$
 */
package com.neiljbrown.java7;

import static org.junit.Assert.*;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.UnsupportedEncodingException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;
import java.util.UUID;
import java.util.logging.Logger;

import org.junit.Test;

/**
 * Examples of the new language features available in Java 7, written as a set of JUnit TestCases.
 * 
 * @see http://openjdk.java.net/projects/coin/
 * @see https://blogs.oracle.com/darcy/entry/project_coin_final_five
 */
public class NewLanguageFeaturesTest {

  private static final Logger logger = Logger.getLogger(NewLanguageFeaturesTest.class.getName());

  /**
   * Java 7 provides improved type inference for instances of generic types. The new diamond operator removes the need
   * to duplicate the declaration of generic types on instantiation. The J7 compiler infers the generic type from the
   * declaration.
   * 
   * @see http://docs.oracle.com/javase/7/docs/technotes/guides/language/type-inference-generic-instance-creation.html
   */
  @Test
  public void testDiamondOperator() {
    // Before
    final Map<Integer, List<String>> map1 = new HashMap<Integer, List<String>>();
    final List<String> list1 = new ArrayList<String>();

    // After - You can substitute the parameterized type of the constructor with an empty set of type parameters (<>):
    final Map<Integer, List<String>> map2 = new HashMap<>();
    final List<String> list2 = new ArrayList<>();

    assertEquals(map1, map2);
    assertEquals(list1, list2);

    // Note that types declared without the diamond, are still raw types -
    @SuppressWarnings("unchecked")
    Map<String, List<String>> myMap = new HashMap();
  }

  /**
   * Java 7 allows you to catch and handle different types of exception in the same catch block, even if they don't
   * share a common superclass, avoiding the need to duplicate error handling logic, or catch a broader class of 
   * exception and re-throw.
   * 
   * @see http://docs.oracle.com/javase/7/docs/technotes/guides/language/catch-multiple.html
   */
  @Test
  public void testMultiCatchBlocks() {
    // Test fixture - Just a class providing a method which throws an expected class of exception 
    class Worker {
      public void doWork() throws IOException, SQLException {
        throw new SQLException("Test");
      }
    }

    Worker w = new Worker();
    try {
      w.doWork();
      // Classes of exception to handle are specified in single catch block, separated by '|' (OR and do not
      // need to share a common superclass
    } catch (IOException | SQLException e) {
      // The caught exception 'e' is final - cannot be reassigned

      // Shared error handling logic...
      assertTrue(e instanceof SQLException || e instanceof IOException);
    }
  }

  /**
   * In Java 6, code needs to be written to ensure that resources such as (JDBC/database and JMS/message broker)
   * connections and I/O streams are closed when they're no longer needed to avoid memory leaks. This was done in the
   * finally clause of a try statement. Java 7 provides a means to automate the closure of such resources removing the
   * need to write this code. Resources can implement the new {@link AutoCloseable} interface, which has a single
   * {@link AutoCloseable#close()} method. They can then be declared in an extended version of the try
   * ("with resources") statement.
   * <p>
   * This is of most value to those frameworks and containers which manage connections for us, but still reduces 
   * boilerplate when writing e.g. file handling code.
   * <p>
   * Resources declared in a try-with-resources statement are always closed regardless of whether the block of code in
   * the try statement completes normally or an exception is thrown (as in a finally clause).
   * <p>
   * In Java 6, when using a finally clause, if the code inside the try block and the code (e.g. reader.close()) 
   * inside the finally clause both throw an exception then the exception thrown in the try block is suppressed and the 
   * (last) exception that occurred in the finally clause is thrown. This is often NOT what is required. In contrast, in 
   * Java 7, if an exception occurs in both the try block and the try-with-resources statement then the exception that 
   * occurred in the try block is thrown/reported. See example below.
   * 
   * @see http://docs.oracle.com/javase/tutorial/essential/exceptions/tryResourceClose.html
   */
  @Test
  public void testAutomaticResourceManagementAkaTryWithResources() throws IOException {

    // Test fixture - Dummy sub-class of OutputStreamWriter to prove that close() is automatically invoked by the JVM 
    // and to simulate errors. (A mock would be better but avoids adding an additional project dependency).
    class MyOutputStreamWriter extends OutputStreamWriter {
      private boolean closed = false;
      private boolean throwOnClose = false;

      public MyOutputStreamWriter(OutputStream out, String charSet) throws UnsupportedEncodingException {
        super(out, charSet);
      }

      public MyOutputStreamWriter(OutputStream out, String charSet, boolean throwOnClose)
          throws UnsupportedEncodingException {
        super(out, charSet);
        this.throwOnClose = throwOnClose;
      }

      // Simulates an exception on closing if instance configured to.
      @Override
      public void close() throws IOException {
        if (this.throwOnClose) {
          throw new IOException("Test. Error closing stream.");
        }
        logger.info("Closing [" + this + "].");
        this.closed = true;
        super.close();
      }

      // Supports testing close() has been invoked on this instance
      public final boolean isClosed() {
        return closed;
      }
    }

    File f = new File(System.getProperty("java.io.tmpdir"), UUID.randomUUID().toString());
    f.deleteOnExit();
    final String fileRecord = "Example showing the syntax of the new try-with-resources language feature in Java 7.\n";
    MyOutputStreamWriter osw1 = null;

    // try-with-resources example -
    // The declaration statement appears within parentheses immediately after the try keyword.
    // You may declare one or more resources in a try-with-resources statement. When multiple resources are declared
    // the close() methods are invoked in the reverse order in which they were instantiated
    // In this example, OutputStreamWriter and BufferedReader both implement java.lang.AutoCloseable
    try (MyOutputStreamWriter osw2 = new MyOutputStreamWriter(new FileOutputStream(f), "UTF-8");
        BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(f), "UTF-8"))) {
      osw2.write(fileRecord);
      osw2.flush();
      assertEquals(fileRecord, br.readLine() + "\n");
      osw1 = osw2;
    }
    // From Java 7 onwards, no need for the finally clause any more
    // finally {
    //   if (br !=null) br.close();    
    //   if (osw2 !=null) osw2.close();
    // }
    assertTrue(osw1.isClosed());

    // try-with-resources - Exception handling behaviour and use of suppressed exceptions -
    // If an exception occurs in _both_ the try block and the try-with-resources statement then it is the exception that
    // occurred in the try block that is thrown. The exception from the try-with-resources statement is suppressed but
    // can be retrieved using the Throwable.getSuppressed() method on the exception thrown in the try block

    // Create an OutputStreamWriter which will throw an exception on close(), within the try-with-resources statement
    boolean throwOnClose = true;
    final String tryBlockErrorMessage = "Test. Error in output stream.";
    try (MyOutputStreamWriter osw2 = new MyOutputStreamWriter(new FileOutputStream(f), "UTF-8", throwOnClose)) {
      // Throw an exception from with the try block also
      throw new NullPointerException(tryBlockErrorMessage);
    } catch (NullPointerException npe) {
      // The fact that an NPE was thrown, and not an IOException, illustrates that Java 7 throws exception reported in
      // the try block in preference to exceptions thrown in the try-with-resources statement
      assertEquals(tryBlockErrorMessage, npe.getMessage());
      // Assert that the exception thrown in the try-with-resources statement is available via Throwable.getSuppressed()
      Throwable[] throwables = npe.getSuppressed();
      assertEquals(1, throwables.length);
      assertEquals(IOException.class, throwables[0].getClass());
      assertEquals("Test. Error closing stream.", throwables[0].getMessage());
    } catch (IOException ioe) {
      fail("Expected the IOException thrown by the OutputStreamWriter.close() method to be suppressed.");
      // A try-with-resources statement can still have catch and/or finally blocks. These are run _AFTER_ the resources
      // have been closed.
    } finally {
      logger.info("Doing final clause");
    }
  }

  /**
   * Strings can now be used in switch statements, in addition to Java 6's support for primitive types and enums.
   * <p>
   * This is one of the less useful language features. Code might be slightly more readable than writing the alternative
   * if-else statements, but enums offer better type safety than strings, and less likely to result in a NPE. When's the
   * last time you wrote a switch statement? One use-case is for processing user input...
   * 
   * @see http://docs.oracle.com/javase/tutorial/java/nutsandbolts/switch.html
   */
  @Test
  public void testSwitchOnString() {
    final String[] options = new String[] { "option A", "option B", "option C" };
    String randomOption = options[new Random().nextInt(options.length)];

    logger.info("testSwitchOnString() - Random option [" + randomOption + "]");
    switch (randomOption) {
    case "option A":
      assertEquals("option A", randomOption);
      break;
    case "option B":
      assertEquals("option B", randomOption);
      break;
    case "option C":
      assertEquals("option C", randomOption);
      break;
    default:
      fail("Unknown option [" + randomOption + "]");
    }

    // switch performs equivalent of String.equals(), so comparison IS case sensitive
    final String uppercaseOption = "OPTION A";
    switch (uppercaseOption) {
    case "option A":
      fail("Unexpected case insenstive matching of option [" + uppercaseOption + "]");
      break;
    case "OPTION A":
      assertEquals("OPTION A", uppercaseOption);
      break;
    default:
      fail("Logic error. No case handling for option [" + uppercaseOption + "].");
    }

    // Check string not null before passing to switch, otherwise an NPE will result
    String[] dodgyOptions = new String[] { null };
    randomOption = dodgyOptions[new Random().nextInt(dodgyOptions.length)];
    try {
      switch (randomOption) {
      default:
        fail("Expected NullPointerException to be thrown.");
        break;
      }
    } catch (NullPointerException expected) {
    }
  }

  /**
   * In Java 7, integer-based types (byte, short, int and long) can be expressed in binary, as well as octal (base 8),
   * decimal (base 10) and hex (base 16), using the '0b' prefix.
   * <p>
   * Mainly of use in low level control or simulation of the actual computer hardware. In cases like these, it is nice
   * to be able to manipulate single bits in places where they represent specific individual signals in the computer
   * system or simulation.
   * 
   * @see http://docs.oracle.com/javase/7/docs/technotes/guides/language/binary-literals.html
   */
  @Test
  public void testBinaryLiterals() {
    // An 8-bit 'byte' value
    byte b = (byte) 0b00100001;
    assertEquals(33, b);    
    // A 32-bit int value, with leading zeros suppressed
    int myAge = (int) 0b10101;
    assertEquals(21, myAge);
  }

  /**
   * Java 7 allows you to include underscores in numeric literals to act as a separator to improve readability, in a
   * similar way in which you would use punctuation marks like a comma or space.
   * 
   * @see http://docs.oracle.com/javase/7/docs/technotes/guides/language/underscores-literals.html
   */
  @Test
  public void testUnderscoresInIntegerAndBinaryLiterals() {
    final long creditCardNumber = 1234_5678_9012_3456L;
    assertEquals("1234567890123456", Long.toString(creditCardNumber));
    final int value = 1_000_000;
    assertEquals(1000000, value);
    // Underscores can be used in binary as well as integer literal
    assertEquals(0b00000001_00000101, 261);
  }
}